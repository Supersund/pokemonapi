export class PokemonEntry {
  id: string;
  name: string;
  sprite: string;
  url: string;

  constructor(id: string, name: string, sprite: string, url: string) {
    this.id = id;
    this.name = name;
    this.sprite = sprite;
    this.url = url;
  }
}
