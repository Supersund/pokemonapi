export class PokemonProfile {
  name: string;
  height: number;
  weight: number;
  abilities: string[];
  spriteUrl: string;

  constructor(pokemonObject) {
    (this.name = pokemonObject.name),
      (this.weight = pokemonObject.weight),
      (this.height = pokemonObject.height),
      (this.abilities = pokemonObject.abilities);
      //this.spriteUrl = 
  }
}
