import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../services/api.service";
import { PokemonEntry } from "../../models/pokemon-entry";
import { getId, getSpriteUrl } from "../../util/stringutils";

@Component({
  selector: "app-pokemon-list",
  templateUrl: "./pokemon-list.component.html",
  styleUrls: ["./pokemon-list.component.scss"]
})
export class PokemonListComponent implements OnInit {
  pokemons: PokemonEntry[] = [];
  next = "";

  constructor(private apiService: ApiService) {}

  addMorePokemons() {
    let resp = this.next
      ? this.apiService.getPokemons(this.next)
      : this.apiService.getPokemons();
    resp.subscribe(data => {
      console.log(data);
      this.next = data["next"];
      data["results"].forEach(element => {
        this.pokemons.push(
          new PokemonEntry(
            getId(element.url),
            element.name,
            getSpriteUrl(element.url),
            element.url
          )
        );
      });
    });
  }

  onClick() {
    this.apiService.getPokemonById("3").subscribe(element => {
      //Do something
    });
  }

  onScroll() {
    console.log("You scrolled to the bottom. Son.");
  }

  ngOnInit() {
    this.addMorePokemons();
  }
}
