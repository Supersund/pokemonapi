import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PokemonListComponent } from "./home/pokemon-list/pokemon-list.component";
import { PokemonProfileComponent } from "./profile/pokemon-profile/pokemon-profile.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found/page-not-found.component";

const routes: Routes = [
  { path: "", component: PokemonListComponent },
  { path: "profile/:id", component: PokemonProfileComponent },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
