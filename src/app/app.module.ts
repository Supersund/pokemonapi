import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PokemonListComponent } from "./home/pokemon-list/pokemon-list.component";
import { PokemonProfileComponent } from "./profile/pokemon-profile/pokemon-profile.component";
import { NavbarComponent } from "./core/navbar/navbar.component";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { PokemonCardComponent } from "./home/pokemon-card/pokemon-card.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonProfileComponent,
    NavbarComponent,
    PokemonCardComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    InfiniteScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
