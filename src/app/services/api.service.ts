import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

const baseUrl: string = "https://pokeapi.co/api/v2/pokemon/";
@Injectable({
  providedIn: "root"
})
export class ApiService {
  constructor(private httpClient: HttpClient) {}

  public getPokemons(url: string = baseUrl) {
    return this.httpClient.get(url);
  }

  public getPokemonById(id: string) {
    return this.httpClient.get(baseUrl + id);
  }
}
