import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { switchMap } from "rxjs/operators";
import { ApiService } from "src/app/services/api.service";

@Component({
  selector: "app-pokemon-profile",
  templateUrl: "./pokemon-profile.component.html",
  styleUrls: ["./pokemon-profile.component.scss"]
})
export class PokemonProfileComponent implements OnInit {
  pokemon$;
  constructor(private route: ActivatedRoute, private service: ApiService) {}

  ngOnInit() {
    this.pokemon$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getPokemonById(params.get("id"))
      )
    );
  }
}
