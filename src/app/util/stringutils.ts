export function getId(pokemonUrl: string): string {
  return pokemonUrl.slice(34, pokemonUrl.length - 1);
}

export function getSpriteUrl(pokemonUrl: string): string {
  return (
    "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
    getId(pokemonUrl) +
    ".png"
  );
}
